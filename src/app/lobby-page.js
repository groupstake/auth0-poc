import React from 'react';

import store from './store';
import { logout } from './actions';


export default class LobbyPage extends React.Component {

    render() {
        return <div className="page lobby-page">
            <header>
                <h1>Lobby</h1>
            </header>
            <section>
                {this.renderUserInfo()}
            </section>
            <footer>
                <button className="logout" onClick={logout}>
                    Logout
                </button>
            </footer>
        </div>;
    }

    renderUserInfo() {
        let user = store.get('user');
        console.log(user);
        return <dl className="user-info">
            <div>
                <img src={user.picture} alt="User picture" style={{width: '100px', height: '100px'}}/>
            </div>
            <div>
                <dt>Email:</dt>
                <dd>{user.email}</dd>
            </div>
            <div>
                <dt>First name:</dt>
                <dd>{user.given_name}</dd>
            </div>
            <div>
                <dt>Last name:</dt>
                <dd>{user.family_name}</dd>
            </div>
        </dl>;
    }
}
