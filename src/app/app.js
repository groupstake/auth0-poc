import React from 'react';

import store from './store';
import LobbyPage from './lobby-page';
import { startSession } from './actions';

import './app.scss';


export default class App extends React.Component {

    componentDidMount() {
        startSession();
        this.observer = store.observer(this.forceUpdate.bind(this));
    }

    render() {
        let user = store.get('user');
        if (user === null)
            return null;
        return <LobbyPage/>;
    }
}
