import auth0 from 'auth0-js';
import store from './store';


let auth = new auth0.WebAuth({
    domain: 'commonstock.auth0.com',
    clientID: 'LaQyCNAtmoMAHFuci5BHsK2J6KFohL08',
    redirectUri: 'http://localhost:8080',
});

export function startSession() {
    if (localStorage.getItem('session'))
        return resumeSession();
    auth.parseHash((error, session) => {
        session ? completeLogin(session) : redirectToLogin();
    });
}

export function logout() {
    localStorage.clear();
    auth.logout({
        returnTo: 'http://localhost:8080',
    });
}

function redirectToLogin() {
    auth.authorize({
        responseType: 'token',
    });
}

function completeLogin(session) {
    let sessionJson = JSON.stringify(session);
    localStorage.setItem('session', sessionJson);
    location.href = '';
}

function resumeSession() {
    let sessionJson = localStorage.getItem('session');
    let session = JSON.parse(sessionJson);
    console.log(session);
    auth.client.userInfo(session.accessToken, (error, user) => {
        if (error)
            return logout();
        store.set('user', user);
    });
}
